from limitlessled import Color
from limitlessled.bridge import Bridge
from limitlessled.group.rgbww import RGBWW
from limitlessled.group.white import WHITE

class Lamp:

  def __init__(self, *args):
    self.bridge = args[0]
    self.mqtt   = args[1]
    self.index  = args[2]
    self.topic  = args[3]
    self.type   = args[4]

    self.subscriptions = [
      { 'topic': str.join('/', ['home', self.topic, 'temperature', 'input']), 'callback': self.setTemperature },
      { 'topic': str.join('/', ['home', self.topic, 'brightness',  'input']), 'callback': self.setBrightness }
    ]

    self.bulb = self.bridge.add_group(self.index, self.topic, self.type)

  # brightness 0 - 100
  def setBrightness(self, b):
    b = int(b)
    if 0 <= b <= 100:
      if b > 0:
        self.turn(b)
        self.bulb.brightness = float(b/100)
      else:
        self.bulb.brightness = float(b/100)
        self.turn(b)

      self.mqtt.publish(str.join('/', ['home', self.topic, 'brightness', 'output']), b)

  # brightness 0 - 100
  def setTemperature(self, t):
    t = int(t)
    if 0 <= t <= 100:
      self.bulb.temperature = float(t/100)
      self.mqtt.publish(str.join('/', ['home', self.topic, 'temperature', 'output']), t)

  # turn on/off 1 - 0
  def turn(self, v):
    self.bulb.on = bool(v)
