import uuid
import paho.mqtt.client as mqtt

class Mqtt:
  def __init__(self):
    self.client = mqtt.Client()
    self.client.on_connect = self.on_connect
    self.client.on_message = self.on_message
    self.subscriptions = []

  def disconnect(self):
    self.client.loop_stop(force = False)

  def on_connect(self, client, userdata, flags, rc):
    for sub in self.subscriptions:
      client.subscribe(sub['topic'])
      print(" - " + sub['topic'])

  def on_message(self, client, userdata, msg):
    self.trigger_subscriptions(msg.topic, msg.payload.decode("utf-8"))

  def trigger_subscriptions(self, topic, message):
    for sub in self.subscriptions:
      if sub['topic'] == topic:
        sub['callback'](message)

  def publish(self, topic, message):
    self.client.publish(topic, message)

  def add_subscriptions(self, subs):
    for sub in subs:
      self.subscriptions.append({
        'topic': sub['topic'],
        'callback': sub['callback']
      })

  def connect(self):
    self.client.connect('192.168.0.129', 1883, 60)
    self.client.loop_forever()
