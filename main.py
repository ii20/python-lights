from modules.lamp import Lamp
from modules.mqtt import Mqtt

from limitlessled.bridge import Bridge
from limitlessled.group.rgbww import RGBWW
from limitlessled.group.white import WHITE

mqtt = Mqtt()

bridge = Bridge('192.168.0.191')

lamp1 = Lamp(bridge, mqtt, 1, 'livingroom/main', RGBWW)
lamp2 = Lamp(bridge, mqtt, 2, 'hallway/main', RGBWW)
lamp3 = Lamp(bridge, mqtt, 3, 'bedroom/main', RGBWW)
lamp4 = Lamp(bridge, mqtt, 4, 'kitchen/main', RGBWW)

mqtt.add_subscriptions(lamp1.subscriptions)
mqtt.add_subscriptions(lamp2.subscriptions)
mqtt.add_subscriptions(lamp3.subscriptions)
mqtt.add_subscriptions(lamp4.subscriptions)
mqtt.connect()
